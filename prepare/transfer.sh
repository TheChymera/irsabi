mkdir -p ../data/manual_overview/generic
cp ~/.scratch/irsabi/manual_overview/generic/coherence_4008_cbv.pdf ../data/manual_overview/generic/ || exit 1
cp ~/.scratch/irsabi/manual_overview/generic/4008_ofMcF1_T2w.pdf ../data/manual_overview/generic/ || exit 1
cp ~/.scratch/irsabi/manual_overview/generic/4008_ofMcF1_cbv.pdf ../data/manual_overview/generic/ || exit 1
cp ~/.scratch/irsabi/manual_overview/generic/VZ01_baseline_bold.pdf ../data/manual_overview/generic/ || exit 1
cp ~/.scratch/irsabi/manual_overview/generic/VZ01_baseline_T1w.pdf ../data/manual_overview/generic/ || exit 1
mkdir -p ../data/manual_overview/legacy_dsurqec
cp ~/.scratch/irsabi/manual_overview/legacy_dsurqec/4008_ofMcF1_cbv.pdf ../data/manual_overview/legacy_dsurqec/ || exit 1
cp ~/.scratch/irsabi/manual_overview/legacy_dsurqec/VZ01_baseline_bold.pdf ../data/manual_overview/legacy_dsurqec/ || exit 1
mkdir -p ../data/manual_overview/legacy
cp ~/.scratch/irsabi/manual_overview/legacy/coherence_4008_cbv.pdf ../data/manual_overview/legacy/ || exit 1
cp ~/.scratch/irsabi/manual_overview/legacy/4008_ofMcF1_cbv.pdf ../data/manual_overview/legacy/ || exit 1
cp ~/.scratch/irsabi/manual_overview/legacy/VZ01_baseline_bold.pdf ../data/manual_overview/legacy/ || exit 1
