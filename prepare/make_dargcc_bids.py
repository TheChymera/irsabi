import numpy as np
import pandas as pd
import os
from samri.pipelines.reposit import bru2bids
from labbookdb.report.selection import animal_id, parameterized
from datetime import datetime

data_dir = '~/ni_data/VZe'
scratch_dir = os.path.expanduser('~/.scratch')
base_dir = '{}/irsabi'.format(scratch_dir)
bru2bids(data_dir,
	inflated_size=False,
	functional_match={
		'acquisition':['EPI'],
		},
	structural_match={
		'acquisition':['FLASH'],
		},
	keep_crashdump=True,
	out_base=base_dir,
	workflow_name='dargcc_bids'
	)
