from samri.pipelines.preprocess import generic, legacy
from samri.pipelines import manipulations

scratch_dir = '~/.scratch/irsabi'

# Preprocess IRSABI data:
bids_base = '{}/bids'.format(scratch_dir)
generic(bids_base,
	'/usr/share/mouse-brain-templates/ambmc_200micron.nii',
	registration_mask='/usr/share/mouse-brain-templates/ambmc_200micron_mask.nii',
	functional_match={'acquisition':['EPIlowcov'],},
	structural_match={'acquisition':['TurboRARElowcov'],},
	out_base='{}/preprocessing'.format(scratch_dir),
	workflow_name='generic_ambmc',
	)
legacy(bids_base,
	'/usr/share/mouse-brain-templates/ldsurqec_200micron_masked.nii',
	functional_match={'acquisition':['EPIlowcov']},
	out_base='{}/preprocessing'.format(scratch_dir),
	workflow_name='legacy_dsurqec',
	n_jobs_percentage=0.6,
	)
generic(bids_base,
	'/usr/share/mouse-brain-templates/dsurqec_200micron.nii',
	registration_mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	functional_match={'acquisition':['EPIlowcov'],},
	structural_match={'acquisition':['TurboRARElowcov'],},
	out_base='{}/preprocessing'.format(scratch_dir),
	workflow_name='generic',
	)
legacy(bids_base,
	'/usr/share/mouse-brain-templates/lambmc_200micron.nii',
	functional_match={'acquisition':['EPIlowcov']},
	out_base='{}/preprocessing'.format(scratch_dir),
	workflow_name='legacy',
	n_jobs_percentage=0.6,
	)

# Preprocess DARGCC data:
dargcc_bids_base = '{}/dargcc_bids'.format(scratch_dir)
generic(dargcc_bids_base,
	'/usr/share/mouse-brain-templates/ambmc_200micron.nii',
	registration_mask='/usr/share/mouse-brain-templates/ambmc_200micron_mask.nii',
	functional_match={'acquisition':['EPI'],},
	structural_match={'acquisition':['FLASH'],},
	out_base='{}/preprocessing'.format(scratch_dir),
	workflow_name='generic_ambmc',
	)
legacy(dargcc_bids_base,
	'/usr/share/mouse-brain-templates/ldsurqec_200micron_masked.nii',
	functional_match={'acquisition':['EPI']},
	out_base='{}/preprocessing'.format(scratch_dir),
	workflow_name='legacy_dsurqec',
	n_jobs_percentage=0.6,
	)
generic(dargcc_bids_base,
	'/usr/share/mouse-brain-templates/dsurqec_200micron.nii',
	registration_mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	functional_match={'acquisition':['EPI'],},
	structural_match={'acquisition':['FLASH'],},
	out_base='{}/preprocessing'.format(scratch_dir),
	workflow_name='generic',
	)
legacy(dargcc_bids_base,
	'/usr/share/mouse-brain-templates/lambmc_200micron.nii',
	functional_match={'acquisition':['EPI']},
	out_base='{}/preprocessing'.format(scratch_dir),
	workflow_name='legacy',
	n_jobs_percentage=0.6,
	)
