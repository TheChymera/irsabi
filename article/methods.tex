\section{Methods}

\subsection{Data}

For the quality control of the workflows, a dataset with an effective size of 164 scans is used.
Scans were acquired using a Bruker PharmaScan system (\SI{7}{\tesla}, \SI{16}{\centi\meter} bore), and variable coils.
Experimental procedures were approved by the Veterinary Office of the Canton of Zurich in accordance with relevant regulations.

All animals were anesthetized for scanning with medetomidine (\SI{0.05}{\milli\gram\per\kilo\gram\per\BW} bolus and \SI{0.1}{\milli\gram\per\kilo\gram\per\BW\per\hour} intraperitoneal infusion) and isoflurane \SI{0.5}{\percent}, the protocol being derived from the previously published “Med/Iso” specification \cite{Grandjean2014}.


\subsubsection{T2w+CBV and T2w+BOLD}

Data was acquired from 11 adult ePet-Cre \cite{Scott2005} C57BL/6 mice
(\py{pytex_printonly('scripts/sex.py')},
\py{pytex_printonly('scripts/age.py')}
old at study onset),
with each animal scanned on up to 5 sessions, at 14 day intervals.
Each session contains an anatomical scan and two functional scans --- with Blood-Oxygen Level Dependent (BOLD) \cite{Ogawa1990} and Cerebral Blood Volume (CBV) \cite{Marota1999} contrast, respectively (for a total of 68 functional scans).

Anatomical scans were acquired via a TurboRARE sequence, with a RARE factor of 8, an echo time (TE) of \SI{21}{\milli\second}, an inter-echo spacing of \SI{7}{\milli\second}, and a repetition time (TR) of \SI{2500}{\milli\second}, sampled sagittally at $\mathrm{\Delta x(\nu)=\SI{166.7}{\micro\meter}}$, horizontally at $\mathrm{\Delta y(\phi)=\SI{75}{\micro\meter}}$, and coronally at $\mathrm{\Delta z(t)=\SI{650}{\micro\meter}}$ (slice thickness of \SI{500}{\micro\meter}).
The field of view covered \SI[product-units=power]{20 x 9}{\mm} and was sampled via a $\mathrm{120 \times 120}$ matrix.
A total of 14 slices were acquired.

The functional BOLD and CBV scans were acquired using a gradient-echo Echo Planar Imaging (ge-EPI) sequence with a flip angle of \SI{60}{\degree} and with $\mathrm{TR/TE = \SI{1000}{\milli\second}/\SI{15}{\milli\second}}$ and $\mathrm{TR/TE = \SI{1000}{\milli\second}/\SI{5.5}{\milli\second}}$, respectively.
Data were sampled at $\mathrm{\Delta x(\nu)=\SI{312.5}{\micro\meter}}$, $\mathrm{\Delta y(\phi)=\SI{281.25}{\micro\meter}}$, and $\mathrm{\Delta z(t)=\SI{650}{\micro\meter}}$ (slice thickness of \SI{500}{\micro\meter}).
The field of view covered \SI[product-units=power]{20 x 9}{\mm} and was sampled via a $\mathrm{64 \times 32}$ matrix.
A total of 14 slices were acquired.

Animals were fitted with an optic fiber implant ($\mathrm{l=\SI{3.2}{\milli\meter} \ d=\SI{400}{\micro\meter}}$) targeting the Dorsal Raphe (DR) nucleus 14 days or longer prior to the first session.
The nucleus was sensitized to optical stimulation by transgenic expression of Cre recombinase under the ePet promoter \cite{Scott2005} and rAAV delivery of a plasmid for Cre-conditional Channelrhodopsin and YFP expression ---
pAAV-EF1a-double floxed-hChR2(H134R)-EYFP-WPRE-HGHpA, reposited by Karl Deisseroth (\href{https://www.addgene.org/20298/}{Addgene plasmid \#20298}).
Scans were acquired using an in-house T/R surface coil, engineered to permit optic fiber protrusion.

The DR was stimulated via an Omicron LuxX 488-60 laser (\SI{488}{\nano\meter}) tuned to \SI{30}{\milli\watt} at contact with the fiber implant, according to the protocol listed in \cref{tab:stim}.
Stimulation was delivered and reports recorded via the COSplay system \cite{cosplay}.

\subsubsection{T1w+BOLD}

Data was acquired from 10 adult male C57BL/6 mice (Jackson laboratory, Maine, USA), which were 34 days old at study onset, with each animal scanned on 3 sessions (repeated at 58 days postnatal and 112 days postnatal) \cite{dargcc}.
Each session contains an anatomical scan and a functional BOLD contrast scan (for a total of 30 functional scans).

Anatomical scans were acquired via a Fast low-angle shot (FLASH) sequence with a RARE factor of 1, a flip angle of \SI{30}{\degree}, and $\mathrm{TR/TE = \SI{522}{\milli\second}/\SI{3.51}{\milli\second}}$.
Data were sampled at $\mathrm{\Delta x(\nu)=\SI{52}{\micro\meter}}$, $\mathrm{\Delta y(\phi)=\SI{26}{\micro\meter}}$, $\mathrm{\Delta z(t)=\SI{500}{\micro\meter}}$ (slice thickness of \SI{500}{\micro\meter}).
The field of view covered \SI[product-units=power]{20 x 10}{\mm} and was sampled via a $\mathrm{384 \times 384}$ matrix.
A total of 20 slices were acquired.

The functional BOLD scans were acquired using a gradient-echo Echo Planar Imaging (ge-EPI) sequence with a flip angle of \SI{60}{\degree} and $\mathrm{TR/TE = \SI{1000}{\milli\second}/\SI{15}{\milli\second}}$.
Data were sampled at $\mathrm{\Delta x(\nu)=\SI{222}{\micro\meter}}$, $\mathrm{\Delta y(\phi)=\SI{200}{\micro\meter}}$, and $\mathrm{\Delta z(t)=\SI{500}{\micro\meter}}$ (slice thickness of \SI{500}{\micro\meter}).
The field of view covered \SI[product-units=power]{20 x 10}{\mm} and was sampled via a $\mathrm{90 \times 50}$ matrix.
A total of 20 slices were acquired.

\subsection{Metrics}

For the VCF we use the 66\textsuperscript{th} intensity percentile of the raw scan as a signal threshold --- as determined by operator inspection to best approximate brain selection by signal intensity, without over-optimization.
It should be noted that the percentile threshold neither can nor needs to select brain voxels exclusively, as the transformation is performed on unmasked data (lower thresholds will simply weigh the metric towards additional non-brain structure conservation, and higher thresholds will simply constrain the amount of brain tissue sampled for comparison).
The arbitrary unit equivalent of this threshold is recorded per-scan and applied to all preprocessing results for that particular scan
 --- \cref{eq:vcf}, where $v$ is the voxel volume in the original space, $v^\prime$ the voxel volume in the transformed space, $n$ the number of voxels in the original space, $m$ the number of voxels in the transformed space, $s$ a voxel value sampled from the vector $S$ containing all values in the original data, and $s^\prime$ a voxel value sampled from the transformed data.

\begin{equation} \label{eq:vcf}
        V\!C\!F
        = \frac{v^\prime\sum_{i=1}^m [s^\prime_i \geq P_{66}(S)]}{v\sum_{i=1}^n [s_i \geq P_{66}(S)]}
        = \frac{v^\prime\sum_{i=1}^m [s^\prime_i \geq P_{66}(S)]}{v \lceil0.66n\rceil}
\end{equation}

The SCF metric measures the pre- and post-processing smoothness ratio, defined as the full-width at half-maximum (FWHM) of the signal amplitude spatial autocorrelation function (ACF \cite{eklund2016cluster}).
As fMRI data have non-Gaussian spatial ACF, we use AFNI \cite{cox1996afni} to fit the following function ---
\cref{eq:acf}, where $r$ is the distance of two amplitude distribution samples, $a$ is the relative weight of the Gaussian, $b$ is the width of the Gaussian, and $c$ the decay of the mono-exponential term \cite{cox2017fmri}.

\begin{equation} \label{eq:acf}
        ACF(r)
        = a * e^{ -r^{2}/ (2 * b^{2}) } + (1 - a) + e^{-r/c}
\end{equation}

Statistical power as per the MS metric is represented by the negative logarithm of first-level p-values.
This produces voxelwise statistical estimates for the probability that a time course could --- by chance alone --- be at least as well correlated with the stimulation regressor as the voxel time course measured.
We compute the per-scan average of these values as seen in \cref{eq:ms}, where $n$ represents the number of statistical estimates in the scan, and $p$ is a p-value.

\begin{equation} \label{eq:ms}
        M\!S = \frac{\sum_{i=1}^n -log(p_i)}{n}
\end{equation}

Two statistical models, including main effects and interaction terms, were evaluated for each metric.
The first details workflow and template effects, and the second details contrast effects.
Multiple comparison correction was performed via the Benjamini-Hochberg method, fixing the false discovery rate (FDR) $\alpha$ at 0.05.

\subsection{Software}

The workflow functions leverage the Nipype \cite{nipype}, FSL \cite{fsl} and ANTs \cite{ants} packages.
Software management relevant for verifying the reproducibility of the software environment and analysis results was performed via neuroscience package install instructions for the Gentoo Linux distribution \cite{ng}.
%For Quality Control we distribute as part of this article workflows using the SciPy \cite{scipy}, pandas \cite{pandas}, matplotlib \cite{matplotlib}, Seaborn \cite{seaborn}, and Statsmodels \cite{statsmodels} packages --- using a heteroscedasticity consistent covariance matrix for top-level statistics \cite{long2000}.
For Quality Control we distribute as part of this article workflows using the SciPy \cite{scipy}, pandas \cite{pandas}, Seaborn \cite{seaborn}, and Statsmodels \cite{statsmodels} packages.

\section{Reproducibility and Resource Availability}

All data is freely available \cite{irsabi_bidsdata,dargcc_bidsdata}, and automatically accessible via the Gentoo Linux package manager.
All code relevant for reproducing this document is freely available \cite{source}, and compliant with the RepSeP specification \cite{repsep} to allow article reproduction based solely on the raw data and top-level code of the article.
