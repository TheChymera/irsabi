\section{The Optimized Workflow}
Processing workflow complexity should be manageable to prospective users with only cursory programming experience.
However, workflow transparency, sustainability, and reproducibility should not be compromised for trivial ancillary features.
We thus abide by the following design guidelines:
(1) \textit{each workflow is represented by a high-level function}, with operator-understandable parameters, detailing operations performed, rather than implementations;
(2) \textit{workflow functions are highly parameterized but include workable defaults}, so users may significantly adapt workflows without editing constituent code;
(3) \textit{graphical or interactive interfaces are avoided}, as they impede reproducibility, encumber the dependency graph, and reduce project sustainability.

\begin{figure*}[h!]
	\vspace{-2.5em}
	\begin{subfigure}{\textwidth}
		\centering
		\includedot[width=\textwidth]{data/metadata_bad}
		\vspace{-3em}
		\caption{
			Ad hoc metadata substitution irreversibly damages data and hinders reuse and sharing at all downstream levels.
			}
		\label{fig:mdb}
	\vspace{-0.75em}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		\includedot[width=\textwidth]{data/metadata}
		\vspace{-3em}
		\caption{
			Nondestructive handling of metadata ensures reusability and easy sharing throughout the analysis process.
			}
		\label{fig:mdg}
	\vspace{-0.4em}
	\end{subfigure}
	\begin{subfigure}{.39\textwidth}
		\centering
		\includedot[width=0.88\textwidth]{data/legacy}
		\vspace{-1.3em}
		\caption{
			“SAMRI Legacy” workflow, based on the \textcolor{mg}{\texttt{antsIntroduction.sh}} function, and including destructive manipulations in nodes colored red.
			}
		\label{fig:wfgl}
	\end{subfigure}\hfill
	\begin{subfigure}{.58\textwidth}
		\centering
		\includedot[width=0.815\textwidth]{data/generic}
		\vspace{-2.2em}
		\caption{
			Non-destructive “SAMRI Generic” workflow, based on the \textcolor{mg}{\texttt{antsRegistration}} function, and including small-animal-specific parameter optimization in nodes colored green.
			}
		\label{fig:wfgg}
	\end{subfigure}
	\vspace{0.2em}
	\caption{
		\textbf{The SAMRI Generic workflow uses fine-tuned animal priors to enhance registration quality while preserving metadata integrity.}
		Directed graphs depict both the overall context of MRI data processing and analysis (\textbf{a},\textbf{b}), as well as the internal structure of the two registration workflows compared in this article (\textbf{c},\textbf{d}), which insert into the broader context at the bold orange arrow positions.
		Technical detail available in \cref{fig:nwfg}.
		}
	\vspace{-0.5em}
	\label{fig:wfg}
\end{figure*}

Given the aforementioned principles, we have constructed two registration workflows:
The “Legacy” workflow (\cref{fig:wfgl}, exhibiting the common practices detailed in the \nameref{sec:bg}~section)
and the novel “Generic” workflow (\cref{fig:wfgg}),
Both workflows start by performing dummy scan correction on the fMRI data and the stimulation events file, based on BIDS \cite{bids} metadata, if available.
The “Legacy” workflow subsequently applies a 10-fold multiplication to the voxel size, and deletes the orientation information from the affine matrix.
Further, dimensions are swapped so that the data matrix matches the RPS (left$\rightarrow$Right, anterior$\rightarrow$Posterior, inferior$\rightarrow$Superior) orientation of the corresponding template (\cref{fig:amb}).
Following these data manipulation steps, a temporal mean is computed, and an empirically determined threshold (\SI{10}{\percent} of the 98\textsuperscript{th} percentile) is applied.
Subsequently, the bias field is corrected and masked using the \textcolor{mg}{\texttt{fast}} and \textcolor{mg}{\texttt{bet}} FSL functions.
The image is then warped into the template space using the \textcolor{mg}{\texttt{antsIntroduction.sh}} ANTs function.
Lastly, affine variants are harmonized.
The “Generic” workflow follows up on dummy scan correction with slice timing correction, computes the temporal mean of the functional scan, and bias corrects the temporal mean --- using the \textcolor{mg}{\texttt{N4BiasFieldCorrection}} ANTs function, with spatial parameters adapted to small animal brain features.
Analogous operations are performed on the structural scan, following which the structural scan is registered to the reference template, and the functional scan temporal mean is registered to the structural scan --- using the \textcolor{mg}{\texttt{antsRegistration}} ANTs function, with spatial parameters adapted to small animal brain features.
The structural-to-template and functional-to-structural transformation matrices are merged and applied in one warp step to the functional data, while the structural data is warped solely via the structural-to-template transformation matrix.

General-purpose small animal support is provided in the node optimizations via scale reduction across all parameters, as well as iteration reduction for registration nodes.
The common features of preclinical models thus supported are the smaller scale, smoother brain surfaces, and potential exclusion of the caudalmost and rostralmost areas of the brain.

The workflow is implemented in Python, with automatically generated Command Line Interfaces (CLIs) available for Bash.
As registration is indispensable to data analysis (rather than a stand-alone process), the workflows are distributed as part of a comprehensive workflow package, SAMRI \cite{samri}.

\subsection{Template Package}

\py{pytex_subfigs(
	[
		{'script':'scripts/dsurqec.py', 'label':'dsu', 'conf':'article/template.conf', 'options_pre':'{.48\\textwidth}',
			'options_pre_caption':'\\vspace{-1.5em}',
			'caption':'The “Generic” template: MR \\textit{and} stereotactic data matrix orientation, standard header with RAS orientation and conserved scale, and histologically meaningful Bregma origin.'
			,},
		{'script':'scripts/ambmc.py', 'label':'amb','conf':'article/template.conf', 'options_pre':'{.48\\textwidth}',
			'options_pre_caption':'\\vspace{-1.5em}',
			'caption':'The “Legacy” template: histological data matrix orientation (shared e.g. by the Allen Brain Institute template), and non-standard header with RPS orientation and inflated scale.'
			,},
		],
	caption='
		\\textbf{The "Generic" template provides canonical orientation and Bregma centering.}
		Illustrated are multiplanar depictions of the "Generic" and "Legacy" $T_2$ contrast mouse brain templates, with slice coordinates centered at zero on all axes.',
	options_pre='\\vspace{-1.2em}',
	options_post='\\vspace{1.6em}',
	label='fig:t',)}

Target template quality is highly consequential to registration performance and suitability as a standard procedure.
For example, an inflated template size mandates according parameters for all functions handling data in affine space.
Further, if template axes are flipped, the first (rigid) registration step may be incorrectly determined and the image deformed to match the template at an incorrect orientation.
Consequently, template quality must be ascertained, and a workflow-compliant default should be provided.

Our recommended template (\cref{fig:dsu}) is derived from the DSURQE template of the Toronto Hospital for Sick Children Mouse Imaging Center \cite{dsu}.
We shift its geometric origin to the Bregma landmark, providing harmonization with histological atlases and surgical procedures.
The template is in the canonical NIfTI orientation, RAS (left$\rightarrow$Right, posterior$\rightarrow$Anterior, inferior$\rightarrow$Superior), and has a coronal slice positioning typical for both MRI and stereotactic surgery.
All of the associated files are identified with the prefix \textcolor{mg}{\texttt{dsurqec}} in the template package.

We bundle the aforementioned MR template with histological templates, derived from the Australian Mouse Brain Mapping Consortium (AMBMC) \cite{amb} and  Allen Brain Institute (ABI) \cite{abi} templates, which provide ample rostrocaudal coverage and serve as a reference for numerous gene expression and projection maps, respectively.
We reorient the AMBMC template from its original RPS orientation to the canonical RAS, and apply an RAS orientation to the orientation-less ABI template after NIfTI conversion from the original NRRD format.
Corresponding files are prefixed with \textcolor{mg}{\texttt{ambmc}} and \textcolor{mg}{\texttt{abi}}, respectively.

Additionally, we provide DSURQEC and AMBMC derived templates, in the widespread but incorrect RPS orientation, with the aforementioned tenfold increase in voxel size.
These are prefixed with \textcolor{mg}{\texttt{ldsurqec}} and \textcolor{mg}{\texttt{lambmc}}, respectively.

All templates are provided at \SI{40}{\micro\meter} and \SI{200}{\micro\meter} isotropic resolutions.
%Due to data size, we distribute \SI{15}{\micro\meter} isotropic versions of templates available at this resolution (AMBMC and ABI) separately, thus producing two packages, \textcolor{mg}{\texttt{mouse-brain-templates}} and \textcolor{mg}{\texttt{mouse-brain-templatesHD}}.
Up-to-date versions of these archives can be reproduced via a script collection written and released for the purpose of this publication \cite{atlases_generator}.

For evaluation, \textcolor{mg}{\texttt{dsurqec}} and \textcolor{mg}{\texttt{ldsurqec}} template variations (same data matrix, matched to the orientation and size requirements of the \cref{fig:wfgg} and \cref{fig:wfgl} workflow functions, respectively) are referred to as the “Generic” template.
Analogously, the \textcolor{mg}{\texttt{ambmc}} and \textcolor{mg}{\texttt{lambmc}} template variations are referred to as the “Legacy” template.

\subsection{Qualitative Operator Inspection}

We complement automated whole-dataset evaluation metrics with convenience functions to ease and improve qualitative inspection.
These functions produce paginated slice-by-slice views of registered data, highlighting two different assessments.
The first view mode highlights single-session registration quality, plotting the registered data as a map, and the target atlas as a contour (\cref{fig:fit,fig:fit1}).
The second highlights multi-session registration coherence, plotting the target template as a map, and colored individual session contours (\cref{fig:coherence}).
