\section{Evaluation}

A major challenge of registration QC is that a perfect mapping from the image to the template is undefined.
Similarity metrics are ill-suited for QC, as they are used internally by registration functions, whose mode of operation is based on optimizing them.
%Extreme similarity score maximization is not a desired outcome, particularly if nonlinear transformations are employed, as this may result in image distortions which should be penalized in QC.
Moreover, similarity metrics are not independent, so similarity score optimization issues cannot be circumvented by selecting a subset of metrics and performing QC via the remainder.
To address this challenge, we developed four alternative evaluation metrics: volume conservation, smoothness conservation, functional analysis, and variance analysis.
In order to mitigate possible differences arising from template features, we use these metrics for multi-factorial analyses --- including both a template and a workflow factor.
In order to evaluate robustness with respect to data contrast, we additionally present a multi-factorial break-down of metric variability given contrast combinations of the functional data (BOLD and CBV) and associated structural data (T1-weighted FLASH and T2-weighted RARE).

\subsection{Volume Conservation}

\begin{sansmath}
\py{pytex_subfigs(
        [
                {'script':'scripts/vcf_violin.py', 'label':'vcv', 'conf':'article/3col.conf', 'options_pre':'{.332\\textwidth}',
                        'options_pre_caption':'\\vspace{-1em}',
                        'caption':'~',
                        },
                {'script':'scripts/scf_violin.py', 'label':'scv', 'conf':'article/3col.conf', 'options_pre':'{.332\\textwidth}',
                        'options_pre_caption':'\\vspace{-1em}',
                        'caption':'~',
                        },
                {'script':'scripts/msa_violin.py', 'label':'msv', 'conf':'article/3col_4items.conf', 'options_pre':'{.325\\textwidth}',
                        'options_pre_caption':'\\vspace{-1em}',
                        'caption':'~',
                        },
                ],
        caption='\\textbf{The SAMRI Generic workflow and template reliably conserve volume and smoothness --- unlike the Legacy workflow and template.}
        Plots of three target metrics, with coloured patch width distribution densities, dashed line means, and dotted line inner quartiles.
        Comparisons across processing and templates include all contrast combinations, comparisons across processing and contrasts include only matching template-workflow combinations.
        ',
        options_pre_caption='\\vspace{-.2em}',
        options_pre='\\vspace{-1em}',
        options_post='\\vspace{-.9em}',
        label='fig:v',
        )}
\end{sansmath}

Volume conservation is based on the assumption that brain volume should remain roughly constant after preprocessing.
Assuming size consistency between data and template, a volume increase may indicate that the brain was stretched to fill template space not covered by the scan, while a volume decrease might indicate that non-brain voxels were introduced into the template brain space.
For this analysis we compute a Volume Conservation Factor (VCF), whereby volume conservation is optimal for $\mathrm{VCF = 1}$.

As seen in \cref{fig:vcv} (left panel), we note that VCF is sensitive to
the workflow (\py{boilerplate.fstatistic('Processing', condensed=True)}),
the template (\py{boilerplate.fstatistic('Template', condensed=True)}),
but not the interaction thereof (\py{boilerplate.fstatistic('Processing:Template', condensed=True)}).
The performance of the Generic SAMRI workflow in conjunction with the Generic template is significantly different from that of the Legacy workflow in conjunction with the Legacy template, yielding a two-tailed p-value of \py{pytex_printonly('scripts/vc_t.py')}.
Moreover, the root mean squared error ratio strongly favours the Generic workflow
($\mathrm{RMSE_{L}/RMSE_{G}\simeq} \py{pytex_printonly('scripts/vc_rmser.py')}$).

Descriptively, we observe that the Legacy level of the template variable introduces a notable volume loss
(VCF of \py{boilerplate.factorci('Template[T.Legacy]')}),
while the Legacy level of the workflow variable introduces a volume gain
(VCF of \py{boilerplate.factorci('Processing[T.Legacy]')}).
Further, we note that there is a very strong variance increase in all applications of the Legacy workflow ---
\py{boilerplate.varianceratio(template='Legacy')}-fold (\py{boilerplate.variances(template='Legacy', padding=True)}) given the Legacy template, and \py{boilerplate.varianceratio(template='Generic')}-fold (\py{boilerplate.variances(template='Generic', padding=True)}) given the Generic template.

With respect to data break-up by contrast (\cref{fig:vcv}, right panel), we see significant effects for the contrast
(\py{boilerplate.fstatistic('Contrast', expression='Processing*Contrast', condensed=True, corecomparison=True, df_path='data/volume.csv', dependent_variable='Volume Conservation Factor')}),
and the processing-contrast interaction
(\py{boilerplate.fstatistic('Processing:Contrast', expression='Processing*Contrast', condensed=True, corecomparison=True, df_path='data/volume.csv', dependent_variable='Volume Conservation Factor')})
factors.
Multiple-comparison corrected post-hoc t-tests reveal that the processing factor remains significant in interaction with the
\py{boilerplate.post_hoc(df_path='data/volume.csv', dependent_variable='Volume Conservation Factor', condensed=True)}
of the contrast factor.


\subsection{Smoothness Conservation}

A further aspect of preprocessing quality is the resulting image smoothness.
Although controlled smoothing is a valuable tool used to increase the signal-to-noise ratio (SNR), uncontrolled smoothing limits operator discretion in the trade-off between SNR and feature granularity, leading to undocumented loss of spatial resolution and thus inferior anatomical alignment \cite{fmriprep}.
To ascertain the extent of uncontrolled smoothing, we compute a Smoothness Conservation Factor (SCF), expressing the ratio between the smoothness of the preprocessed images and the smoothness of the original images.
Similarly to the VCF metric, perfect smoothness conservation is given by $\mathrm{SCF = 1}$, though deviations are unavoidalbe due to interpolation and are further impacted by volume effect.

With respect to the data shown in \cref{fig:scv} (left panel), we note that SCF is sensitive to
the template (\py{boilerplate.fstatistic('Template', condensed=True, df_path='data/smoothness.csv', dependent_variable='Smoothness Conservation Factor')}),
the workflow (\py{boilerplate.fstatistic('Processing', condensed=True, df_path='data/smoothness.csv', dependent_variable='Smoothness Conservation Factor')}),
but not the interaction of the factors (\py{boilerplate.fstatistic('Processing:Template', condensed=True, df_path='data/smoothness.csv', dependent_variable='Smoothness Conservation Factor')}).
The performance of the Generic SAMRI workflow in conjunction with the Generic template is significantly different from that of the Legacy workflow in conjunction with the Legacy template, yielding a two-tailed p-value of \py{pytex_printonly('scripts/scf_t.py')}.
In this comparison, the root mean squared error ratio favours the Generic workflow
($\mathrm{RMSE_{L}/RMSE_{G}\simeq} \py{pytex_printonly('scripts/scf_rmser.py')}$).

Descriptively, we observe that the Legacy level of the template variable introduces a smoothness reduction
(SCF of \py{boilerplate.factorci('Template[T.Legacy]', df_path='data/smoothness.csv', dependent_variable='Smoothness Conservation Factor')}),
while the Legacy level of the workflow variable introduces a smoothness gain
(SCF of \py{boilerplate.factorci('Processing[T.Legacy]', df_path='data/smoothness.csv', dependent_variable='Smoothness Conservation Factor')}).
Further, we note that there is a strong variance increase for the Legacy workflow ---
\py{boilerplate.varianceratio(df_path='data/smoothness.csv',dependent_variable='Smoothness Conservation Factor', max_len=3, template='Legacy')}-fold (\py{boilerplate.variances(df_path='data/smoothness.csv',dependent_variable='Smoothness Conservation Factor', max_len=3, template='Legacy')}) given the Legacy template, and \py{boilerplate.varianceratio(df_path='data/smoothness.csv',dependent_variable='Smoothness Conservation Factor', template='Generic', max_len=3)}-fold (\py{boilerplate.variances(df_path='data/smoothness.csv',dependent_variable='Smoothness Conservation Factor', max_len=3, template='Generic')}) given the Generic template.

Given the break-up by contrast shown in \cref{fig:scv} (right panel), we see significant effects for the contrast factor
(\py{boilerplate.fstatistic('Contrast', expression='Processing*Contrast', condensed=True, corecomparison=True, df_path='data/smoothness.csv', dependent_variable='Smoothness Conservation Factor')})
and the processing-contrast interaction
(\py{boilerplate.fstatistic('Processing:Contrast', expression='Processing*Contrast', condensed=True, corecomparison=True, df_path='data/smoothness.csv', dependent_variable='Smoothness Conservation Factor')}).
Multiple-comparison corrected post-hoc t-tests reveal that the processing factor remains significant in interaction with the
\py{boilerplate.post_hoc(condensed=True)}
of the contrast factor.

\subsection{Functional Analysis}

Functional analysis is a frequently used avenue for preprocessing QC, and has some viability as it is a feature not weighted by the registration process.
However, the method is computationally intensive and only applicable to stimulus-evoked functional data.
Additionally, functional analysis significance scores are sensitive to data smoothness \cite{Molloy2014}, and thus an increased score on account of uncontrolled smoothing can be expected.
For this analysis we compute the Mean Significance (MS), expressing the significance detected across all voxels of a scan, for the subset of contrasts which feature stimulus-evoked activity (T2w+CBV and T2w+BOLD).

As seen in \cref{fig:msv} (left panel), MS is sensitive to
the workflow
(\py{boilerplate.fstatistic('Processing', dependent_variable='Mean Significance', df_path='data/functional_significance.csv', condensed=True)}),
but not to the template
(\py{boilerplate.fstatistic('Template', dependent_variable='Mean Significance', df_path='data/functional_significance.csv', condensed=True)}),
or the interaction of the factors
(\py{boilerplate.fstatistic('Processing:Template', dependent_variable='Mean Significance', df_path='data/functional_significance.csv', condensed=True)}).

The performance of the SAMRI Generic workflow (with the Generic template) differs significantly from that of the Legacy workflow (with the Legacy template) in terms of MS, yielding a two-tailed p-value of \py{pytex_printonly('scripts/ms_t.py')}.

Descriptively, we observe that the Legacy level of the workflow variable introduces a notable significance increase
(MS of \py{boilerplate.factorci('Processing[T.Legacy]', df_path='data/functional_significance.csv', dependent_variable='Mean Significance')}),
while the Legacy level of the template variable
(MS of \py{boilerplate.factorci('Template[T.Legacy]', df_path='data/functional_significance.csv', dependent_variable='Mean Significance')}),
and the interaction of the Legacy template and Legacy workflow
(MS of \py{boilerplate.factorci('Processing[T.Legacy]:Template[T.Legacy]', df_path='data/functional_significance.csv', dependent_variable='Mean Significance')})
introduce no significance change.
Furthermore, we again note a variance increase in all applications of the Legacy workflow
Further, we note that there is a strong variance increase for the Legacy workflow ---
\py{boilerplate.varianceratio(df_path='data/functional_significance.csv',dependent_variable='Mean Significance', max_len=3, template='Legacy')}-fold (\py{boilerplate.variances(df_path='data/functional_significance.csv',dependent_variable='Mean Significance', max_len=3, template='Legacy')}) given the Legacy template, and \py{boilerplate.varianceratio(df_path='data/functional_significance.csv',dependent_variable='Mean Significance', template='Generic', max_len=3)}-fold (\py{boilerplate.variances(df_path='data/functional_significance.csv',dependent_variable='Mean Significance', max_len=3, template='Generic')}) given the Generic template.

With respect to data break-up by contrast (\cref{fig:msv}, right panel), we see no notable main effect for the contrast variable
(\py{boilerplate.fstatistic('Contrast', expression='Processing*Contrast', condensed=True, corecomparison=True, df_path='data/functional_significance.csv', dependent_variable='Mean Significance')}),
nor the contrast-template interaction
(\py{boilerplate.fstatistic('Processing:Contrast', expression='Processing*Contrast', condensed=True, corecomparison=True, df_path='data/functional_significance.csv', dependent_variable='Mean Significance')}).

Functional analysis effects can further be inspected by visualizing population-level maps.
Second-level t-statistic maps depicting the T2w+CBV and T2w+BOLD omnibus signal (common to all subjects and sessions) provide a succinct overview capturing signal amplitude, directionality, and coverage (\cref{fig:m}).
%While the most salient feature of this figure is the qualitative distribution difference between CBV and BOLD scans, we note that this is to be expected given the different nature of the processes, and is wholly orthogonal to the question of registration.
%Crucial to the examination of registration quality and its effects on functional read-outs is the differential coverage.
We note that the Legacy workflow induces coverage overflow (\cref{fig:mllc,fig:mlgb,fig:mllb}), while the Legacy template causes acquisition slice misalignment (\cref{fig:mglc,fig:mllc,fig:mglb,fig:mllb}).
Positive activation of the Raphe system, most clearly disambiguated from the surrounding tissue in the T2w+BOLD contrast, is notably displaced very far caudally by the joint effects of the Legacy workflow and the Legacy template (\cref{fig:mllb}).
Processing with the Generic template and workflow (\cref{fig:mggc,fig:mggb}) successfully mitigates all of these issues.

\py{pytex_subfigs(
	[
		{'script':'scripts/map_generic_cbv.py', 'label':'mggc', 'conf':'article/map.conf', 'options_pre':'{.488\\textwidth}',
                        'options_pre_caption':'\\vspace{-1.55em}',
                        'options_post':'\\vspace{-.1em}',
			'caption':'
                                Generic workflow with Generic template T2w+CBV map: correct slice orientation and coverage bounds.
                                '
			,},
		{'script':'scripts/map_generic_ambmc_cbv.py', 'label':'mglc','conf':'article/map.conf', 'options_pre':'{.488\\textwidth}',
                        'options_pre_caption':'\\vspace{-1.55em}',
                        'options_post':'\\vspace{-.1em}',
			'caption':'
                                Generic workflow with Legacy template T2w+CBV map: distorted slice orientation and correct coverage bounds.
                                '
			,},
		{'script':'scripts/map_legacy_dsurqec_cbv.py', 'label':'mlgc','conf':'article/map.conf', 'options_pre':'{.488\\textwidth}',
                        'options_pre_caption':'\\vspace{-1.55em}',
                        'options_post':'\\vspace{-.1em}',
			'caption':'
                                Legacy workflow with Generic template T2w+CBV map: distorted slice orientation and correct coverage bounds.
                                '
			,},
		{'script':'scripts/map_legacy_cbv.py', 'label':'mllc','conf':'article/map.conf', 'options_pre':'{.488\\textwidth}',
                        'options_pre_caption':'\\vspace{-1.55em}',
                        'options_post':'\\vspace{-.1em}',
			'caption':'
				Legacy workflow with Legacy template T2w+CBV map: distorted slice orientation and overflowing bounds.
				'
			,},
		{'script':'scripts/map_generic_bold.py', 'label':'mggb', 'conf':'article/map.conf', 'options_pre':'{.488\\textwidth}',
                        'options_pre_caption':'\\vspace{-1.55em}',
                        'options_post':'\\vspace{-.1em}',
			'caption':'
                                Generic workflow with Generic template T2w+BOLD map: correct slice orientation and coverage bounds.
                                '
			,},
		{'script':'scripts/map_generic_ambmc_bold.py', 'label':'mglb','conf':'article/map.conf', 'options_pre':'{.488\\textwidth}',
                        'options_pre_caption':'\\vspace{-1.55em}',
                        'options_post':'\\vspace{-.1em}',
			'caption':'
                                Generic workflow with Legacy template T2w+BOLD map: distorted slice orientation and correct coverage bounds.
                                '
			,},
		{'script':'scripts/map_legacy_dsurqec_bold.py', 'label':'mlgb','conf':'article/map.conf', 'options_pre':'{.488\\textwidth}',
                        'options_pre_caption':'\\vspace{-1.55em}',
                        'options_post':'\\vspace{-.1em}',
			'caption':'
                                Legacy workflow with Generic template T2w+BOLD map: correct slice orientation and overflowing coverage.
                                '
			,},
		{'script':'scripts/map_legacy_bold.py', 'label':'mllb','conf':'article/map.conf', 'options_pre':'{.488\\textwidth}',
                        'options_pre_caption':'\\vspace{-1.55em}',
                        'options_post':'\\vspace{-.1em}',
                        'caption':'
				Legacy workflow with Legacy template T2w+BOLD map: distorted slice orientation and overflowing coverage.
			        '
                        ,},
		],
	caption='
                \\textbf{Generic processing eliminates map overflow into adjacent anatomical regions, and Generic template usage reduces slice orientation distortions.}
                As seen in multiplanar depictions of second-level omnibus statistic maps, thresholded at $\mathrm{|t|\geq1}$.
                The acquisition area is bracketed in pink, and comparison with statistic coverage should account for the latter being more constrained, as the omnibus statistic contrast is only defined for voxels captured in \\textit{every} evaluated scan.
                ',
        options_pre='\\vspace{-1.4em}',
        options_post='\\vspace{-.4em}',
	label='fig:m',)}

\subsection{Variance Analysis}

\begin{sansmath}
\py{pytex_fig('scripts/variance_catplot.py',
        conf='article/varplot.conf',
        label='var',
        caption='
                \\textbf{The SAMRI Generic workflow conserves subject-wise variability and minimizes trial-to-trial variability compared to the Legacy workflow.}
                Swarmplots illustrate similarity metric scores between preprocessed images and the corresponding workflow template, plotted across subjects (x-axis bins) and sessions (individual points in each x-axis bin), for the CBV contrast.
                ',
        options_post='\\vspace{-1.0em}',
        options_pre_caption='\\vspace{-1.3em}',
        multicol=True,
        )}
\end{sansmath}

An additional assessment of preprocessing quality focuses on the robustness to longitudinal data variability, and whether this is attained without overfitting (i.e. compromising physiologically meaningful variability).
The core assumption of this analysis is that registration should be less susceptible to experiment repetition and aging over an 8-week period in adult mice, than to inter-individual differences.
Consequently, similarity scores of preprocessed scans with respect to the target template should show more variation across subject variable levels than session variable levels.
This comparison can be performed using a type 3 ANOVA of a general linear model fit including a subject and a session level, and with with heteroscedasticity-consistent covariance estimators \cite{mackinnon1985some}.

For this assessment we select three metrics:
Neighborhood Cross Correlation (CC, sensitive to localized correlation),
Global Correlation (GC, sensitive to whole-image correlation),
and Mutual Information (MI, sensitive to whole-image information similarity).
%As similarity metrics are not equivalent, and since the main comparison of variance is performed across all of them, p-values for the variable effects are uncorrected.
For this comparison we select animals in the mature and middle aged brackets (3 to 14 months) \cite{age}, within which inter-session variability is more clearly distinguished as physiologically less meaningful than inter-subject variability.
We further target this descriptive statistical analysis which is based on the distribution and therefore ample presence of variance, at the matching workflow-template comparison for the T2w+CBV contrast, which shows the strongest effect in both VCF and SCF.

\Cref{fig:var} renders similarity scores for the SAMRI Generic and Legacy workflows (considering only the matching workflow-template combinations).
The SAMRI Legacy workflow and template combination produces results with higher F-statistics for the session than for the subject variable, across all metrics:
CC (subject: \py{boilerplate.variance_test('C(Subject)','Legacy','CC', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Legacy','CC', condensed=True)}),
GC (subject: \py{boilerplate.variance_test('C(Subject)','Legacy','GC', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Legacy','GC', condensed=True)}),
and MI (subject: \py{boilerplate.variance_test('C(Subject)','Legacy','MI', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Legacy','MI', condensed=True)}).

For the SAMRI Generic workflow this pattern reverses in 2 of the 3 assessed metrics, with overall higher F-statistic scores for the subject variable than for the session variable:
CC (subject: \py{boilerplate.variance_test('C(Subject)','Generic','CC', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Generic','CC', condensed=True)}),
GC (subject: \py{boilerplate.variance_test('C(Subject)','Generic','GC', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Generic','GC', condensed=True)}),
and MI (subject: \py{boilerplate.variance_test('C(Subject)','Generic','MI', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Generic','MI', condensed=True)}).

%\Cref{fig:var} renders similarity scores for the SAMRI Generic and Legacy workflows (considering only the matching workflow-template combinations).
%The Legacy workflow produces results with predominantly higher F-statistics for the session than the subject variable:
%CC (subject: \py{boilerplate.variance_test('C(Subject)','Legacy','CC', acquisition='bold', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Legacy','CC', acquisition='bold', condensed=True)}),
%GC (subject: \py{boilerplate.variance_test('C(Subject)','Legacy','GC', acquisition='bold', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Legacy','GC', acquisition='bold', condensed=True)}),
%and MI (subject: \py{boilerplate.variance_test('C(Subject)','Legacy','MI', acquisition='bold', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Legacy','MI', acquisition='bold', condensed=True)}).
%
%The Generic SAMRI workflow shows a reversing trend, with overall higher F-statistic scores for the subject variable than for the session variable:
%CC (subject: \py{boilerplate.variance_test('C(Subject)','Generic','CC', acquisition='bold', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Generic','CC', acquisition='bold', condensed=True)}),
%GC (subject: \py{boilerplate.variance_test('C(Subject)','Generic','GC', acquisition='bold', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Generic','GC', acquisition='bold', condensed=True)}),
%and MI (subject: \py{boilerplate.variance_test('C(Subject)','Generic','MI', acquisition='bold', condensed=True)}, session: \py{boilerplate.variance_test('C(Session)','Generic','MI', acquisition='bold', condensed=True)}).
%
