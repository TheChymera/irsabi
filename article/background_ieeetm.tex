\section{Background}
\label{sec:bg}

\IEEEPARstart{C}{orrespondence} of brain areas across individuals is a prerequisite of generalizable statements regarding brain function and organization.
This is achieved by spatial transformation of brain maps in a study to a population or study reference template.
This process, called registration, is integral to any neuroimaging workflow attempting to produce results which are both spatially resolved and meaningful at the population level.

The computations required for registration are commonly performed in the preprocessing workflow,
though image manipulation may only take place once inter-subject comparison becomes needed.
As a consequence of this peripheral positioning in the data evaluation sequence, and of the independence from experimental designs and hypotheses, registration is often relegated to default values and exempt from rigorous design efforts and QC.

Registration in human brain imaging benefits from high-level functions (e.g. \textcolor{mg}{\texttt{flirt}} and \textcolor{mg}{\texttt{fnirt}} from the FSL package\cite{fsl}, or \textcolor{mg}{\texttt{antsIntroduction.sh}} from the ANTs package\cite{ants}), optimized for the size and spatial features of the human brain.
In small animal brain imaging, registration is performed via the selfsame high-level functions as human brain imaging --- rendered usable by adjusting the data to fit function parameters, rather than vice-versa.
This approach compromises data veracity, limits optimization potential, and represents a notable hurdle for the methodological improvement of small animal brain imaging.

Ad-hoc workflows for preclinical imaging often emerge as the need mandates, but lack broad reusability due to highly variable protocols for data acquisition across centers \cite{carp2012secret}.
While further advancements are being made for general-purpose human data preprocessing \cite{fmriprep}, the preclinical field still lacks similarly reliable tools.
Recently, a preclinical registration framework has been released \cite{anderson2019small}, dealing specifically with structural recordings focused on voxel-based morphometry (VBM) and validated via phantom-based metrics.
General-purpose registration workflows applicable both to functional and structural data remain lacking, as do benchmarking efforts which take into account QC metric challenges arising from the heterogeneity of in vivo experiments.

Below, we explicitly describe current practices used in extant ad-hoc workflows, in an effort to not only propose better solutions, but do so in a falsifiable manner which provides adequate detail for both novel and the legacy methods.

\subsection{Manipulations}
The foremost data manipulation procedure in present-day small animal MRI is scale adjustment.
Scale is represented by the Neuroimaging Informatics Technology Initiative format (NIfTI) \cite{nifti} affine transformation parameters, which map the data matrix to spatial coordinates.
Commonly, this manipulation constitutes a 10-fold increase in each spatial dimension.

In order to produce acceptable results from brain extraction based on human priors, it may be necessary to additionally adjust the data matrix content itself.
This may involve applying an ad-hoc intensity-based percentile threshold to delete non-brain as well as anterior and posterior brain voxels, and leave a more spherical brain for human masking functions to operate on.
While conceptually superior solutions adapting parameters to animal data are available \cite{rbet,Oguz2014} and might remove the need for this step of data adaptation, rudimentary solutions remain popular.
Both these function adaptations for animal data and the animal data matrix content adaptations for use with human brain extraction functions are, however, prone to completely or partially remove the olfactory bulbs.
For this reason, the choice is sometimes made to simply forego brain extraction.

Scan orientation metadata may be seen as problematic, and consequently deleted.
This consists in resetting the NIfTI S-Form affine to zeroes, and is intended to mitigate orientations incompatible with the target template.
While it is true that scanner-reported affine spaces for small animal data may be nonstandard (the confusion is two-fold: small animals lie prone with the coronal plane progressing axially whereas higher primates lie supine with the horizontal plane progressing axially), affine spaces of small animal brain templates may be nonstandard as well.
A related manipulation is dimension swapping, which changes the order of the NIfTI data matrix dimensions rather than the affine metadata.
Thus, correct or automatically redressable affine parameters can be deleted and data degraded beyond easy recovery, in order to correspond to a malformed template.

\subsection{Templates}
As the above demonstrates, the template is a key dependency for registration.
Templates used for small animal brain MRI registration are heterogeneous and include histological, as well as ex vivo MRI templates, scanned either inside the intact skull or after physical brain extraction.

Histological templates benefit from high spatial resolution and, in the mouse model in particular, access to molecular information in the same coordinate space.
Such templates are not produced in volumetric sampling analogous to MRI, are hard to register to due to their contrast, may be severely deformed due to their extraction process, and are often not assigned a meaningful affine transformation after conversion to NIfTI.
Data registered to templates thus deformed is challenging to use for navigating the intact brain during stereotactic surgery.

Ex vivo templates based on extracted brains share most of the deformation issues present in histological templates;
they are, however, available in MR contrasts, making registration more reliable.
Ex vivo templates based on intact heads provide both MR contrast and brains largely free of deformation and supporting whole brain registration.
%Independently of brain extraction, MR templates need to have any histological or molecular information relevant for downstream analysis first registered to them.

\subsection{Challenges}
The foremost challenges in small animal MRI registration consist in eliminating data-degrading workarounds, reducing reliance on high-level interfaces with inappropriate optimizations, and improving the quality of standard space templates.
Information loss (pertaining to both the affine and the data matrix) is a particularly besetting issue, since the loss of data at the onset of a neouroimaging workflow  persists throughout downstream steps and precludes numerous modes of analysis (\cref{fig:mdb} vs. \cref{fig:mdg}).
Additionally, given data heterogeneity in various MRI applications, processing effects need to be evaluated with respect to at least the most common contrasts, such as anatomical $T_1$ and $T_2$-weighted imaging, as well as variations of functional contrast, such as cerebral blood volume (CBV) and blood-oxygen-level-dependent (BOLD) imaging.
