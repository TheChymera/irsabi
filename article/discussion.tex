\section{Discussion}

The workflow and template design presented herein represent a significant methodological improvement in preclinical MRI, with both prospective and retroactive applicability, in conjunction with BIDS data or vendor data and vendor-to-BIDS \cite{aowsis} conversion workflows.
The workflow offers significant advantages by reducing coverage overestimation and uncontrolled smoothing, and by improving session-to-session consistency.
This is most prominently highlighted by Volume Conservation (\cref{fig:vcv}), Smoothness Conservation (\cref{fig:scv}), and Variance Analysis (\cref{fig:var}), where the combined usage of the SAMRI Generic workflow and template outperforms all other combinations of the multi-factorial analysis.
Increased region assignment validity is also revealed in a qualitative examination of higher-level functional maps (\cref{fig:m}), where only the Generic workflow and template combination provides accurate coverage of the sampled volume for stimulus-evoked fMRI data.
%These benefits are provided without constraining statistical power (\cref{fig:msv}) in excess of what may be expected due to smoothness conservation.

These benefits are robust with respect to tested contrast combinations (\cref{fig:v}), with the Generic workflow-template combination being less or equally susceptible to the contrast variable compared to the Legacy workflow-template combination.
The performance of the Generic workflow is more consistent across all metrics, as demonstrated by notable standard deviation reductions for VCS, SCF, and MS.
Variance analysis reveals that the Generic workflow in conjunction with the Generic template improves registration robustness --- with respect to to session-specific preparation and positioning, as well as adult animal age progression over a period of 8 weeks --- and better conserves subject-specific features, as compared to the Legacy workflow and template combination.

The Benchmarking data collection, which includes animals with optogenetic implants, showcases workflow robustness with respect to both magnetic susceptibility artefacts (arising from scar tissue around the dental cement attachment), as well as volumetric distortion resulting from brain lesion and implant presence.
Further, this last feature reveals reliable multi-session implant position conservation for the Generic (\cref{fig:coherence}) but not the Legacy (\cref{fig:coherence_legacy}) workflow.
Robustness to further sources of data variability is substantiated by SNR heterogeneity in the benchmarking data collection, which is acquired not only with different scan protocols and with or without a contrast agent, but also with different coils (in-house T/R surface coil for T2w+BOLD and T2w+CBV, and cryogenic coil for T1w+BOLD).
In fact, the surface coil protocol constitutes a challenging and unfavourable setting for registration due to its intrinsic $B_1$ gradient.
In spite of this, registration remains robust.
Lastly, the registration pipeline contains a bias field correction step which is optimized to provide significantly more aggressive behaviour than the default parameters --- with respect to both its spatial frequency threshold and amplitude gain.
This renders further homogeneity variations (in the spatial resolution range obtained by using different field strengths or resonator coils) mathematically equivalent to SNR variations.

Closer model inspection reveals that in addition to the processing factor, the template factor also drives variability.
The Legacy template induces both volume and smoothness decrease below raw data values (\cref{fig:vcv,fig:scv}).
This clearly indicates a whole-volume effect, whereby a target template smaller than the recoded brain causes contraction during registration, affecting both volume and smoothness.
%This effect can also be observed qualitatively in \cref{fig:m}.
We thus highlight the importance of an appropriate template choice, and strongly recommend usage of the Generic template on account of better scale similarity to data acquired in adult mice.

The volume conservation, smoothness conservation, and session-to-session consistency of the SAMRI Generic workflow and template combination are further complemented by numerous design benefits (\cref{fig:wfg,fig:t}).
These include increased transparency and parameterization (easing inspection and further customization), veracity of resulting data headers, and spatial coordinates more meaningful for surgical interventions.

\subsection{Quality Control}

A major contribution of this work is the implementation of multiple metrics providing simple and thorough QC for registration performance (VCF, SCF, MS, and Variance Analysis); and the release of a data collection \cite{irsabi_bidsdata,dargcc_bidsdata} suitable for multifactorial benchmarking.

The VCF and SCF provide good quantitative estimates of distortion prevalence.
The analysis comparing subject-wise and session-wise variance is a conceptually interesting avenue for ascertaining how much a registration workflow is potentially overfitting.
It should, however, primarily be understood as a descriptive statistical metric and re-used with caution, as it is highly sensitive to heteroscedasticity and whether or not the covariance estimator appropriately accounts for it.
These metrics are relevant to both preclinical and clinical MRI workflow improvements.
%and could themselves be further optimized.
%One particular metric refinement we propose is developing percentile selection heuristics based on a priori documented data distortions for VCF, as metric sensitivity may be contingent on the percentile, and the present choice of the \nth{66} percentile may not have the highest sensitivity.

Global statistical power is not a reliable metric for registration optimization.
Regrettably, however, it may be the most prevalently used if results are only inspected at a higher level --- and could bias analysis.
This is exemplified by the positive main effect of the Legacy workflow seen in \cref{fig:msv}.
In this particular case, optimizing for statistical power alone would give a misleading indication, as becomes evident when all metrics are inspected.

We suggest that a VCF, SCF and Variance based comparison, coupled with visual inspection of a small number of omnibus statistic maps is a feasible and sufficient tool for benchmarking workflows.
We recommend reuse of the presented data for workflow benchmarking, as they include (a) multiple sources of variation (contrast, session, subjects), (b) functional activity with broad coverage but spatially distinct features, and (c) significant distortions due to implant properties --- which are appropriate for testing workflow robustness.
In addition to the workflow code \cite{samri}, we openly release the re-executable source code \cite{source} for all statistics and figures in this document.
Thus both the novel method and the benchmarking process are fully transparent and reusable with further data.

\subsection{Conclusion}

We present a novel registration workflow, “SAMRI Generic”, which offers several advantages compared to ad hoc approaches commonly used for small animal MRI.
In-depth multivariate comparison with a thoroughly documented Legacy pipeline revealed superior performance of the SAMRI Generic workflow in terms of volume and smoothness conservation, as well as variance structure across subjects and sessions.
The metrics introduced for registration QC are not restricted to the processing of small animal fMRI data, and can be readily expanded to other brain imaging applications.
The optimized registration parameters of the SAMRI Generic Workflow are easily accessible in the source code and transferable to any other workflows making use of the ANTs package.
%The open source software choices in both the workflow and this article's source code permit users to better verify, understand, remix, and reuse our work.
Overall, we believe that the SAMRI Generic workflow should facilitate and harmonize processing of mouse brain imaging data across studies and centers.
