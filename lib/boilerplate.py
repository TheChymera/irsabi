import itertools
import numpy as np
import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf
from copy import deepcopy
from os import path
from lib.utils import float_to_tex, inline_anova, inline_factor
from statsmodels.stats.multitest import multipletests

def fstatistic(factor,
	df_path='data/volume.csv',
	dependent_variable='Volume Conservation Factor',
	expression='Processing*Template',
	exclusion_criteria={},
	corecomparison=False,
	**kwargs
	):
	df_path = path.abspath(df_path)
	df = pd.read_csv(df_path)

	df = df.loc[df['Processing']!='Unprocessed']
	if corecomparison:
		df = df.loc[((df['Processing']=='Legacy') & (df['Template']=='Legacy')) | ((df['Processing']=='Generic') & (df['Template']=='Generic'))]

	for key in exclusion_criteria.keys():
		df = df.loc[~df[key].isin(exclusion_criteria[key])]

	formula='Q("{}") ~ {}'.format(dependent_variable, expression)
	ols = smf.ols(formula, df).fit()
	anova = sm.stats.anova_lm(ols, typ=3)
	tex = inline_anova(anova, factor, 'tex', **kwargs)
	return tex

def factorci(factor,
	df_path='data/volume.csv',
	dependent_variable='Volume Conservation Factor',
	expression='Processing*Template',
	exclusion_criteria={},
	**kwargs
	):
	df_path = path.abspath(df_path)
	df = pd.read_csv(df_path)

	df = df.loc[df['Processing']!='Unprocessed']

	for key in exclusion_criteria.keys():
		df = df.loc[~df[key].isin(exclusion_criteria[key])]

	formula = 'Q("{}") ~ {}'.format(dependent_variable, expression)
	model = smf.mixedlm(formula, df, groups='Uid')
	fit = model.fit()
	summary = fit.summary()
	tex = inline_factor(summary, factor, 'tex', **kwargs)
	return tex

def corecomparison_factorci(factor,
	df_path='data/volume.csv',
	dependent_variable='Volume Conservation Factor',
	expression='Processing*Contrast',
	exclusion_criteria={},
	**kwargs
	):
	df_path = path.abspath(df_path)
	df = pd.read_csv(df_path)

	df = df.loc[df['Processing']!='Unprocessed']
	df = df.loc[((df['Processing']=='Legacy') & (df['Template']=='Legacy')) | ((df['Processing']=='Generic') & (df['Template']=='Generic'))]

	for key in exclusion_criteria.keys():
		df = df.loc[~df[key].isin(exclusion_criteria[key])]

	formula = 'Q("{}") ~ {}'.format(dependent_variable, expression)
	model = smf.mixedlm(formula, df, groups='Uid')
	fit = model.fit()
	summary = fit.summary()
	tex = inline_factor(summary, factor, 'tex', **kwargs)
	return tex

def varianceratio(
	df_path='data/volume.csv',
	template=False,
	dependent_variable='Volume Conservation Factor',
	max_len=2,
	**kwargs
	):

	df_path = path.abspath(df_path)
	df = pd.read_csv(df_path)

	df = df.loc[df['Processing']!='Unprocessed']

	if template:
		df = df.loc[df['Template']==template]
	legacy = np.var(df.loc[df['Processing']=='Legacy', dependent_variable].tolist())
	generic = np.var(df.loc[df['Processing']=='Generic', dependent_variable].tolist())


	ratio = legacy/generic

	return float_to_tex(ratio, max_len, **kwargs)

def variances(
	df_path='data/volume.csv',
	template=False,
	dependent_variable='Volume Conservation Factor',
	max_len=2,
	**kwargs
	):

	df_path = path.abspath(df_path)
	df = pd.read_csv(df_path)

	df = df.loc[df['Processing']!='Unprocessed']

	if template:
		df = df.loc[df['Template']==template]
	legacy = np.var(df.loc[df['Processing']=='Legacy', dependent_variable].tolist())
	generic = np.var(df.loc[df['Processing']=='Generic', dependent_variable].tolist())


	legacy_tex = float_to_tex(legacy, max_len, **kwargs)
	generic_tex = float_to_tex(generic, max_len, **kwargs)

	output = "variances of {} and {} for the Legacy and Generic workflows, respectively".format(legacy_tex, generic_tex)

	return output

def variance_test(
        factor,
        workflow,
        metric,
        df_path='data/variance.csv',
        template=False,
        max_len=2,
	acquisition='cbv',
	**kwargs
        ):
        df = pd.read_csv(path.abspath(df_path))
        df = df.loc[df['Processing']==workflow]
        #contrast
        df = df.loc[df['acquisition'].str.contains(acquisition)]
        model= metric + '~ C(Subject) + C(Session)'
        ols = smf.ols(model, df).fit()
        anova = sm.stats.anova_lm(ols, typ=3, robust='hc3')
        tex = inline_anova(anova,  factor, 'tex', **kwargs)
        return tex

def post_hoc(
	df_path='data/smoothness.csv',
	core_comparison=True,
	dependent_variable= 'Smoothness Conservation Factor',
	formula = 'Q("{}") ~ Contrast + Processing:Contrast',
	debug_full_formula = False,
	inter_level_comparisons=True,
	levels = [
		'Processing[T.Legacy]:Contrast[T2w+BOLD]',
		'Processing[T.Legacy]:Contrast[T2w+CBV]',
		'Processing[T.Legacy]:Contrast[T1w+BOLD]',
		],
	reporting_levels = [
		'T2w+BOLD',
		'T2w+CBV',
		'T1w+BOLD',
		],
        max_len=2,
        **kwargs
	):
	"""Return a document-ready listing of specified post-hoc t-test p-values.

	Parameters
	----------

	df_path : str, optional
		Path to a CSV file with columns including variable names as specified in the `dependent_variable` and `formula` parameters.
	core_comparison : bool, optional
		Whether to restrict the comparison to the core cases (matching Processing and Template pairs).
		This parameter is IRSABI-specific.
	dependent_variable : str, optional
		Dependent variable of the model
	formula : str, optional
		A formattable Python string (including `{}`) according to statsmodels syntax, which expresses the desired reporting formula.
		Note that in order to give interaction-level estimates, this formula needs to NOT model one of the main factors, so that its variance is only evaluated based on the interaction with the other factor.
		This means that if you are reporting how the effects of factor B vary along levels of factor C, the correct formula is not `'Q("{}") ~ B*C'`, but `'Q("{}") ~ C + B:C'`.
	debug_full_formula : str, optional
		A formattable Python string (including `{}`) according to statsmodels syntax, which expresses the full formula for the model you are reporting on.
		In the aforementioned case this would indeed probably be `'Q("{}") ~ B*C'`.
		This feature is useful to verify that the f-contrast you are detailing is indeed matching the value obtained in the full model, and setting this option will trigger debug output printing.
		This option should not be set for usage within a document.
	inter_level_comparison : bool, optional
		Whether to compute the significance for comparisons between the levels.
		This is to be used if you want to determine the significance not for the comparison of the levels with zero, but between the levels, i.e. it is significant that level B[1]:C[1] differs from level B[1]:C[2].
	max_len : int, optional
		Maximum length for decimal reporting.
	levels : list of str, optional
		A list of the explicit levels of the model for which the post-hoc t-tests should be performed.
		Valid values can be read from the first column of the fit summary which is printed when the function is run with `debug_full_formula` set.
	reporting_levels : list of str, optional
		A list of the level names as you would like to have them printed in the report.
		The list should have the same length as `levels`, and the reporting level names should be given in the same order.
	"""


	df = pd.read_csv(path.join(df_path), index_col=False)
	if core_comparison:
		df = df.loc[((df['Processing']=='Legacy') & (df['Template']=='Legacy')) | ((df['Processing']=='Generic') & (df['Template']=='Generic'))]

	if debug_full_formula:
		debug_full_formula = debug_full_formula.format(dependent_variable)
		model = smf.ols(debug_full_formula, df)
		fit = model.fit()
		summary = fit.summary()
		anova_summary = sm.stats.anova_lm(fit, typ=3)
		print(anova_summary)
		print(summary)

	formula = formula.format(dependent_variable)
	model = smf.ols(formula, df)
	fit = model.fit()
	summary = fit.summary()
	anova_summary = sm.stats.anova_lm(fit, typ=3)

	comparisons = deepcopy(levels)
	f_contrast = deepcopy(levels)
	if inter_level_comparisons:
		extended_comparisons = [[a,b] for a, b in itertools.combinations(levels,2)]
		extended_f_contrast = ['{} - {}'.format(a[0],a[1]) for a in extended_comparisons]
		comparisons.extend(extended_comparisons)
		f_contrast.extend(extended_f_contrast)
	f_contrast = ','.join(f_contrast)
	f = fit.f_test(f_contrast)

	contrasts = []
	for comparison in comparisons:
		if isinstance(comparison, str):
			contrast = [1 if i == comparison else 0 for i in fit.params.index.to_list()]
			contrasts.append(contrast)
		else:
			contrast = [int(i in comparison) for i in levels]
			contrast = [1 if i in comparison else 0 for i in fit.params.index.to_list()]
			second = False
			for ix, i in enumerate(contrast):
				if i==1 and second:
					break
				elif i == 1:
					second=True
			contrast[ix] = -1
			contrasts.append(contrast)

	t_tests = fit.t_test(np.array(contrasts))
	g, corrected_pvalues, _, _ = multipletests(t_tests.pvalue, alpha=0.05, method='fdr_bh')

	if debug_full_formula:
		legend=''
		for ix, p in enumerate(corrected_pvalues):
			legend += 'c{} = {}:\n\tBenjamini-Hochberg corrected p={}\n'.format(ix,comparisons[ix],p)
			print('\n')
			print(f.__str__())
			print('\n\n')
			print(legend)
			print('\n')
			print(t_tests.__str__())
			print('\n')

	significant_list=[]
	nonsignificant_list=[]
	for ix,p in enumerate(corrected_pvalues):
		if comparisons[ix] not in levels:
			continue
		d={}
		d['p'] = float_to_tex(p, max_len, **kwargs)
		levels_index = levels.index(comparisons[ix])
		d['level'] = comparisons[ix]
		d['reporting_level'] = reporting_levels[ix]
		if p <= 0.05:
			significant_list.append(d)
		else:
			nonsignificant_list.append(d)

	reporting = ''

	for ix, i in enumerate(significant_list):
		reporting+='{} ($p\!=\!{}$)'.format(i['reporting_level'],i['p'])
		if ix+1 == len(significant_list)-1:
			reporting+=', and '
		elif ix+1 < len(significant_list):
			reporting+=', '
	if len(nonsignificant_list) != 0:
		if len(significant_list) == 1:
			reporting += ' level'
		else:
			reporting += ' levels'
		reporting+=', but not the '
		for ix, i in enumerate(nonsignificant_list):
			reporting+='{} ($p\!=\!{}$)'.format(i['reporting_level'],i['p'])
			if ix+1 == len(nonsignificant_list)-1:
				reporting+=', and '
			elif ix+1 < len(nonsignificant_list):
				reporting+=', '
		if len(nonsignificant_list) == 1:
			reporting += ' level'
		else:
			reporting += ' levels'
	else:
		reporting += ' levels'

	return reporting
