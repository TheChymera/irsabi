SERVER='dreamarticles'
WEBSITE='articles.chymera.eu'
[[ ! -z "$HOSTNAME" ]] && HOSTNAME="${HOSTNAME}_"
rsync -avP article.pdf ${SERVER}:${WEBSITE}/${HOSTNAME}irsabi.pdf &&\
	echo "Article uploaded to http://${WEBSITE}/${HOSTNAME}irsabi.pdf"
rsync -avP article_ieeetm.pdf ${SERVER}:${WEBSITE}/${HOSTNAME}irsabi_ieeetm.pdf &&\
	echo "Article uploaded to http://${WEBSITE}/${HOSTNAME}irsabi_ieeetm.pdf"
