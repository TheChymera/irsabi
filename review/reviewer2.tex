\reviewersection

\setcounter{point}{-1}

\begin{point}
	I strongly encourage the authors to address the major issues, particularly those regarding the variance analysis, either in a discussion of the workflow's limitations or, where possible, by adding more detail to the methods and evaluations.
\end{point}
\begin{reply}
	We thank the reviewer for his ample feedback, which has prompted us to reexamine and considerably improve both the content and presentation of our work.
\end{reply}

\begin{point}
	 Differences in variance are repeatedly reported in the text but the standard deviation numbers are never given. It would be preferable to report the standard deviations themselves rather than their relative differences (i.e., "x-fold") because if baseline variance is small,
using only relative differences can be misleading. While relative change is useful for understanding the difference between the two pipelines, if there is only room for one set of values, the priority should be to the absolute values, from which the relative changes can be
calculated by the reader.
\end{point}
\begin{reply}
	We have extended our statistical reporting to include all the additionally required metrics.
\end{reply}

\begin{point}
	The post-hoc t-tests are reported with only the p-values, not the size (or direction) of the difference between the two factors. For example, in the Smoothness Conservation section, all post-hoc t-tests show significant differences between the two workflows, but T1w + BOLD
(and arguably the T2w + BOLD) are not necessarily improved in the generic workflow.
\end{point}
\begin{reply}
    The observation that two-tailed p-values in the absence of discernible directionality (and more generally, effect amplitudes) can be ambiguous is correct and highly pertinent to good standards of statistical reporting.
    It is, however, not true that either the amplitude or directionality is obscured in our reporting.
    Both are clearly discernible in the left hand panels of figure \textbf{3a-c}, where the post-hoc t-test comparisons are presented one per column, showing not only the means and inner quartiles --- but also distribution densities.
    As the reviewer has correctly noted based on these figures, it is true that the processing effect amplitudes for the SCF metric in the T1w+BOLD contrast, and for the VCF metric in the T2w+CBV contrast, are low, as are in fact the reported significance levels.
    Though it would be trivial to extend the verbose reporting output, we would argue that it would overly encumber a section of the text, which is already highly dense in statistical outputs, without enhancing the interpretation of results beyond what can already be gleaned from the figures.
\end{reply}

\begin{point}
	The legacy template description is unclear. In the template package section, it states that ambmc and lambmc template variations are referred to as the legacy template for evaluation. However, in fig. 2b the legacy template is described as having a "histological contrast",
but the version of the AMBMC template downloaded in the code is generated from ex vivo mouse brain MR images at 16.4T. (from `ambmc-c57bl6-model-symmet\_v0.8-nii/README`). Are there missing steps that give this MRI-derived template a histological contrast?
\end{point}
\begin{reply}
	We thank the reviewer for identifying this error, which we have now corrected.
\end{reply}

\begin{point}
	The dataset used for benchmarking is released for public use, but basic information about the dataset is missing from the methods, e.g., the strain(s?) and suppliers are not given. Moreover, it is implied in the methods that the "animals" are mice, but as the title of the
paper refers to "small animal imaging", the phrasing is ambiguous. Were the animals anaesthetised for the scanning? If so, how? Were they restrained?
\end{point}
\begin{reply}
	We have added all the requested data documentation to the methods section, and resolved the ambiguous terminology.
\end{reply}

\begin{point}
	The evaluation of the variance is the weakest section and is not as thoroughly described as the other evaluations with respect to the statistics applied or the data used.
	\textbf{(a)} While I somewhat agree with the premise, "registration should be less susceptible to experiment repetition and aging over an 8-week period in adult mice, than to inter-individual differences", it over-simplifies the sources of variance in a longitudinal study, in my opinion.
If the mice used are wild-type littermates of an inbred strain from a commercial breeder, the inter-individual differences would be very small. Additionally, 'adulthood' in rodents constitutes a larger proportion of the lifespan than in humans, so one would expect that the magnitude
of variance between scans 8 weeks apart would depend on rate of maturation, which itself varies by life-stage, even in adulthood (e.g., young adult, middle-age, early senescence; see: https://www.jax.org/news-and-insights/jax-blog/2017/november/when-are-mice-considered-old).
Therefore, a study of young adult mice may expect some growth between sessions 8 weeks apart, while a study of middle-age mice that have reached a
growth plateau would have a smaller difference between sessions. The age of the animals used to generate the reference template can also influence the variance between sessions, because if the template is demonstrably closer to one life-stage than the other (e.g, a template derived
from 3-month-old mice, being used with data collected from mice at 5 and 7-months-old), one would expect greater variance between the data from the latter session and the template.
    \textbf{(b)} The evaluation appears to be carried out on a subset of the available data. According to the `boilerplate.variancei\_test` function from the accompanying bitbucket, the analysis only uses the the CBV data, but this is not discussed in the text so the reader assumes the
evaluation uses the same data as the previous sections. Moreover, if it is a subset of the data, the exclusion of the BOLD data is confusing, as BOLD variance numbers are available in `data/variance.py`.
    \textbf{(c)} Relatedly, there appears to be unequal numbers of sessions for each subject, there should be clarification in the text whether the (assumed, although not explicitly stated) repeated measures ANOVA has addressed this, as RM ANOVAs are not robust to missing data. Furthermore
the inter-session comparison method is not stated (i.e., are sessions variances compared consecutively or pairwise? If pairwise, are all sessions compared to baseline or are all sessions compared to each other?).
    \textbf{(d)} The variance analysis only considered matching workflow-template combinations, unlike previous sections, but this deviation is not justified in the text. By only considering matching combinations, the difference in variance cannot be solely assigned to the workflow, as the
variance could arise from the choice of registration template. For example, the age of the animals that generated the legacy template is not given, so it could be that the generic template shows an average brain closer in age to the test data, there could be inter-strain
differences, or the similarity measures could be picking up on the alleged differences in contrast between the two templates.
    \textbf{(e)} Assuming that the variance is driven solely by the workflow, figure 5 does seem to show more variability across sessions than across subjects for the legacy workflow, but it would be beneficial to many readers to more clearly explain the similarity value on the y-axis of
figure 5 (e.g., is a value of 0 optimal?). Having said that, the ANOVA results do not convince me that the "trends" for generic and legacy workflows are largely different. Of the three metrics tested, GC alone shows significant effects of subject, but not session, in the generic
workflow variance, compared to the significant effects of session, but not subject, in the legacy workflow. However, CC shows significant effects of subject and session variability in both workflows, suggesting substantial session variance despite workflow choice. Although MI shows
a significant effect of session in the legacy workflow but not the generic workflow, there is not a significant effect of subject variability in
either workflow, suggesting that this metric is not sensitive to the "physiologically meaningful" variance that the metric is intended to preserve. Unfortunately, an increased effect of subject variability compared to session variability in one similarity metric does not make a
convincing "trend".
    Given these concerns, the "guarantee" of session to session consistency in the discussion seems a stretch, and overall this evaluation does not meet the standard set by the previous, more-thoroughly described sections.
\end{point}
\begin{reply}
    Image similarity indeed poses the greatest challenges with respect to evaluation among the presented metrics, as its maximization (unlike that of VCF or SCF) would constitute overfitting and thus compromise feature preservation.
    We detail this limitation as we introduce the more limiting variance evaluation, and we have added further clarifications which address the specific points raised by the reviewer.
    \textbf{(a)} We fully agree with the observation of the reviewer, and this motivates our choice of data selection for this further analysis method.
    We now explicitly state that we limit the analysis to animals which have reached a plateau of brain growth, and provide an appropriate reference for the choice.
    \textbf{(b)} This is motivated by the different age brackets covered by the different data archives which we benchmark our workflow across.
    \textbf{(c)} We use an type 3 ANOVA for a linear model fit which does not enforce a specific hierarchy of repeated measures.
    This is because both factor variances should be jointly maximized to afford qualitative comparability.
    While missing data is not per se biasing in this setting, it is correct that it engenders heteroscedasticity, which in turn can bias the variance estimation for the factor.
    To avoid this we have now recomputed the modelling output using an improved jacknife-derived covariance matrix estimator, shown by MacKinnon and White (1985) to be superior to the initial approach by White (1980), which we previously used --- particularly for small sample sizes.
    Sessions are thereby compared to the baseline intercept for variance estimation.
    \textbf{(d)} The reason for this deviation is that we previously already showed the increased robustness and performance enhancement of the generic workflow-template combination. Subsequently, we introduced the variance analysis as an additional descriptive metric based on this \textit{a priori} assumption. We acknowledge that this analysis is more explorative than the previous ones and motivated by the general lack of good metrics to assess registration quality and prevent overfitting. We elaborate on this point in 2.5e.
    \textbf{(e)} Following the improvement of our statistical modelling based on the concerns raised by the reviewer, we find that the trend previously documented has become much clearer, and better mirrors the effects seen by visual inspection of the figure.
    Even in light of this, we agree that the general point raised regarding the robustness of this measure is valid, and have correspondingly moderated the language of its discussion.
    Given the lack of evaluation metrics for registration in the field, it is nonetheless worthwhile to report on this analysis concept, as well as to recommend its cautiously usage and potential refinement.
    This metric should be viewed as a descriptive statistic that can aid the experimenter in the quality control of the registration.
    We included this concern in our revised manuscript.
\end{reply}

\subsection*{Minor}

\setcounter{point}{0}
\renewcommand{\pointprefix}{m}

\begin{point}
	Title and body refer to small-animal MRI registration, but the paper only includes mouse workflows. I recommend adjusting the title to be more specific.
\end{point}
\begin{reply}
	The toolkit presented provides general-purpose functionality for small animal brain registration --- and is named accordingly in order to facilitate its discovery through web search and other means, as well as in order to avoid artificially limiting its scope in usage and development.
	The in-depth benchmarking showcased in the article was performed on mice, and thus the terminology is adjusted in the repsecitve secions.
	We consider it important for the general-purpose design of the toolkit to be preserved in easily recognizable form in the title.
\end{reply}

\begin{point}
	Writing style can be overly formal, meaning some parts require several reads for comprehension.
\end{point}
\begin{reply}
	We apologise for this, however, without specific indications we find this difficult to address, as it appears to us to be the most concise style for the topic matter.
\end{reply}

\begin{point}
	Authors write "registration is ... exempt from rigorous design efforts and QC" - I disagree that registration is exempt from QC, because even visual assessment of registration outputs is QC, and this is likely to go unreported in a manuscript although it almost certainly
	takes place. Perhaps a more clear statement would be that it is exempt from quantified QC efforts, as registration is generally assessed qualitatively?
	Writing style can be overly formal, meaning some parts require several reads for comprehension.
\end{point}
\begin{reply}
	This is correct, and we have updated the formulation to address the reviewer's observation.
\end{reply}

\begin{point}
	Figure 3 caption says mean is a solid line and IQ are dashed lines, but mean is dashed line and IQs are dotted lines.
\end{point}
\begin{reply}
	We thank the reveiwer for identifying this error, wich we have now corrected.
\end{reply}

\begin{point}
	Smoothness conservation refers to fig. 3b right panel twice, when presumably the first should be left panel.
\end{point}
\begin{reply}
	We thank the reveiwer for identifying this error, wich we have now corrected.
\end{reply}

\begin{point}
	While the optimal value of VCF is stated and the interpretations of $VCF > 1$ and $VCF < 1$ are given in the text, the optimal values of the other metrics are not explained with the same detail. For example, many of the mean SCF values $> 1$; does this mean the pipelines are
implementing 'uncontrolled sharpening'?
\end{point}
\begin{reply}
	We are touching upon this point in the discussion (“This clearly indicates a whole-volume effect, whereby a target template smaller than the recoded brain causes contraction during registration, affecting both volume and smoothness.”) but have now also added a comment introducing this concern where we explain the metric.
\end{reply}

\begin{point}
	The pipeline's bias field correction step is described as aggressive, but it is unclear what makes it so (and what it is being compared to - aggressive compared to what?)
\end{point}
\begin{reply}
	We have now clarified this point, specifying that “aggressive” refers to a low spatial threshold and a high adjustment of amplitudes.
	This is with respect to the “default” usage of the backend function, as well as to casual usage which we have observed in the field but cannot explicitly reference, as methodological transparency is generally reduced for this step of data preparation --- which is a core issue we seek to address in this article.
	The exact parameters are openly accessible and can be found in the custom-node module (\texttt{samri.pipelines.nodes}), or located by querrying the source code for the keyword “bias”.
	They are, however, relative to scale and can thus not be casually explained in the document text.
\end{reply}

\begin{point}
	Although great work has been carried out across studies with different image acquisition protocols and contrasts, it has not been carried out on multi-centre data such as making use of the data available from openneuro.org. This should be addressed as a limitation, as many
centres or retrospective studies may have limited field-of-view, or other idiosyncrasies that have not been tested here, including alternative artefacts that are caused by use of higher field magnets.
\end{point}
\begin{reply}
	Our data includes field-of view variability and, as correctly stated by the reviewer, significant artefacts.
	While the data collection can indeed always be broader, more categories of idiosyncrasies would require an additionally convoluted multi-factorial design (the analysis is already multi-factorial), which would render the interpretation of evaluation results increasingly intransparent.
	We argue that surface-coil $B_1$ variation and implant-based artefacts constitute a particularly challenging application, in excess of that presented by enhanced magnetic susceptibility artefacts --- as would result from higher field applications.
	An example of magnetic susceptibility artefacts, and the fact that they do not begin to affect registration quality, can be seen in the comparison between figures \textbf{S2a} and \textbf{S2b} along the ventral aspect.
\end{reply}

\begin{point}
	Is there a reason why the 66th percentile is chosen for the VCF metric?
\end{point}
\begin{reply}
	We have now clarified this point in the respective section.
	A simple number (66, as opposed to e.g. 68, or 62.5) was picked analogously to the “nothing-up-my-sleeve” concept, in order to indicate that this is a minimally-consequential choice, and not a result of metric parameter over-optimization.
\end{reply}

\begin{point}
	fMRIprep is referenced twice with two separate indices
\end{point}
\begin{reply}
	We thank the reveiwer for identifying this error, wich we have now corrected.
\end{reply}

\begin{point}
	Supplemental figure captions are repetitive, and the implantation slices could be made more clear (in figs s3 and s4) with the a box or an arrow around the slice(s) to be evaluated, to help readers without strong anatomical knowledge (show readers where you want them to
look!).
\end{point}
\begin{reply}
	Figures \textbf{S3} and \textbf{S4} have repetitive captions, as they are analogous in the image description, but best presented on individual pages.
	In these figures we aim to showcase the exact output which can be expected from the package and thus consider it important to not add extraneous highlighting elements.
	We have added a clarification (different for each figure given the orientation), to help readers locate the feature.
\end{reply}

