#!/usr/bin/env bash

TARGET="${1}"
WHITELIST="
	article.tex
	article_general.tex
	article_ieeetm.tex
	poster.tex
	slides.tex
	review.tex
	review1.tex
	"

if [[ $TARGET = "all" ]] || [[ "$TARGET" == "" ]]; then
	for ITER_TARGET in *.tex; do
		if [[ $WHITELIST =~ (^|[[:space:]])$ITER_TARGET($|[[:space:]]) ]];then
			ITER_TARGET=${ITER_TARGET%".tex"}
			./compile.sh ${ITER_TARGET}
		fi
	done
else
	pdflatex -shell-escape ${TARGET}.tex &&\
	pythontex.py ${TARGET}.tex &&\
	pdflatex -shell-escape ${TARGET}.tex &&\
	if [[ $TARGET = "article_ieeetm"  ]]; then
		biber ${TARGET}
	else
		bibtex ${TARGET}
	fi
	pdflatex -shell-escape ${TARGET}.tex &&\
	pdflatex -shell-escape ${TARGET}.tex
	if [[ $TARGET = "article_ieeetm" ]]; then
		./split.sh
	fi
fi
