import pandas as pd
from os import path
import numpy as np
from lib.categorical import violinplot
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import gridspec

gs = gridspec.GridSpec(1, 2, width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1], sharey = ax0)

palette = ['#80e050','#755575']
df_path = path.abspath('data/functional_significance.csv')
df = pd.read_csv(df_path)

ax = violinplot(
        x="Processing",
	y='Mean Significance',
        data=df,
        hue="Template",
        saturation=1,
        split=True,
        inner='quartile',
        palette=palette,
        scale='area',
        dodge=False,
        inner_linewidth=0.75,
        linewidth=mpl.rcParams['grid.linewidth']/1.25,
        linecolor='w',
	ax=ax0,
        )
ax.legend(title='Template', loc='upper left')

# Style
palette = ['#ffb66d','#009093']

df = df.loc[((df['Processing']=='Legacy') & (df['Template']=='Legacy')) | ((df['Processing']=='Generic') & (df['Template']=='Generic'))]

ax = violinplot(
        x="Contrast",
	y='Mean Significance',
        data=df,
        hue="Processing",
        saturation=1,
        split=True,
        inner='quartile',
        palette=palette,
        scale='area',
        dodge=False,
        inner_linewidth=.75,
        linewidth=mpl.rcParams['grid.linewidth']/1.25,
        linecolor='w',
	ax=ax1,
        )

plt.setp(ax1.get_yticklabels(), visible=False)
ax1.set(ylabel=None)
ax0.set_ylabel(ylabel='Mean Significance',labelpad=mpl.rcParams['axes.labelpad']-2.6)
