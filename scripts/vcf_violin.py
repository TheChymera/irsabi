import matplotlib.pyplot as plt
import pandas as pd
import matplotlib as mpl
from os import path
from itertools import product
from lib.categorical import violinplot
from matplotlib import gridspec

gs = gridspec.GridSpec(1, 2, width_ratios=[1,1.45])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1], sharey = ax0)

# Style
palette = ['#80e050','#755575']

volume_path = path.abspath('data/volume.csv')
df1 = pd.read_csv(volume_path)

df1.loc[df1['Processing']=='Unprocessed', 'Template'] = ''
ax = violinplot(
	x="Processing",
	y='Volume Conservation Factor',
	data=df1.loc[df1['Processing']!='Unprocessed'],
	hue="Template",
	saturation=1,
	split=True,
	inner='quartile',
	palette=palette,
	scale='area',
	dodge=False,
        inner_linewidth=0.75,
        linewidth=mpl.rcParams['grid.linewidth']/1.25,
	linecolor='w',
	ax=ax0,
	)
ax.legend(title='Template', loc='upper left')

# Style
palette = ['#ffb66d','#009093']

volume_path = path.abspath('data/volume.csv')
df = pd.read_csv(volume_path)

df = df.loc[df['Processing']!='Unprocessed']
df = df.loc[((df['Processing']=='Legacy') & (df['Template']=='Legacy')) | ((df['Processing']=='Generic') & (df['Template']=='Generic'))]

df.loc[df['Processing']=='Unprocessed', 'Template'] = ''
ax = violinplot(
	x="Contrast",
	y='Volume Conservation Factor',
	data=df,
	hue='Processing',
	saturation=1,
	split=True,
	inner='quartile',
	palette=palette,
	scale='area',
	dodge=False,
        inner_linewidth=0.75,
        linewidth=mpl.rcParams['grid.linewidth']/1.25,
	linecolor='w',
	ax=ax1,
	)

plt.setp(ax1.get_yticklabels(), visible=False)
ax1.set(ylabel=None)
